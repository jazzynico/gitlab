---
redirect_to: 'https://docs.gitlab.com/runner/install/kubernetes-agent.html'
---

This document was moved to [another location](https://docs.gitlab.com/runner/install/kubernetes-agent.html).

<!-- This redirect file can be deleted after <2021-04-22>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->